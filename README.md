<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [skc\_aws\_terraform\_k8s\_hard\_way](#skc_aws_terraform_k8s_hard_way)
- [Keeping notes on building the AWS infrastructure with Terraform](#keeping-notes-on-building-the-aws-infrastructure-with-terraform)
  - [Diagram](#diagram)

<!-- markdown-toc end -->


# skc_aws_terraform_k8s_hard_way



# Keeping notes on building the AWS infrastructure with Terraform

## Diagram

- [aws architecture](./graphic/skc_test_one.drawio.html)


